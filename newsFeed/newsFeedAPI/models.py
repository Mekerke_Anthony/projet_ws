from django.db import models


class Author(models.Model):
    last_name = models.CharField(max_length=250)
    first_name = models.CharField(max_length=250)

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


class Source(models.Model):
    name = models.CharField(max_length=250)
    website = models.URLField()

    def __str__(self):
        return '%s' % self.name


class Article(models.Model):
    title = models.CharField(max_length=250)
    publication_date = models.DateTimeField(auto_now_add=True)
    description = models.TextField()
    image_link = models.URLField()
    content = models.TextField()
    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    author = models.ForeignKey(Author, on_delete=models.CASCADE)

    def __str__(self):
        return '%s' % self.title

class Doc(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField()
    method = models.CharField(max_length=10)
    endpoint = models.CharField(max_length=250)
    curl_data = models.TextField(max_length=250)
    example = models.CharField(max_length=250)
    response = models.TextField()

    def __str__(self):
        return '%s' % self.title