"""
Role: convert queryset into native Python datatypes
to then being convert to JSON or XML ...
"""

from django.contrib.auth.models import User
from rest_framework import serializers

from .models import Author, Source, Article


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['last_name', 'first_name']


class SourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Source
        fields = ['name', 'website']

class ArticleSerializer(serializers.ModelSerializer):
    #author = serializers.SerializerMethodField(read_only=True) #, source="author"
    #def get_author(self, obj):
    #    return obj.author.last_name

    author = AuthorSerializer(required=False)
    #source = SourceSerializer(required=False)

    class Meta:
        model = Article
        fields = ['title', 'publication_date', 'description', 'image_link', 'content', 'author']
        depth = 1

    def create(self, validated_data):
        author_data = validated_data.pop('author')
        author = Author(**author_data)
        author.save()
        article = Article(**validated_data)
        article.author = author
        article.source = Source.objects.get(id=1)
        article.save()
        return article

    def update(self, instance, validated_data):
        #author_data = validated_data.pop('author')
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.image_link = validated_data.get('image_link', instance.image_link)
        instance.content = validated_data.get('content', instance.content)
        #instance.author = author_data
        instance.save()
        return instance