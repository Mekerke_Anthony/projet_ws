from django.template import Library

register = Library()

@register.filter
def curl_data_to_json_data(value):
    return value.replace('\\"','\"').replace('\"{', '{').replace('}\"', '}')