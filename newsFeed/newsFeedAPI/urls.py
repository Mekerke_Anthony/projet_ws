from django.urls import path, include
from . import views

from rest_framework import routers
router = routers.DefaultRouter()
router.register('authors', views.AuthorView)
router.register('sources', views.SourceView)
router.register('articles', views.ArticleView)

app_name = 'newsFeedAPI'
urlpatterns = [
    path('', include(router.urls)),
    path('docs/', views.docs)
]
