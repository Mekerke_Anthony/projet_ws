from django.apps import AppConfig


class NewsfeedapiConfig(AppConfig):
    name = 'newsFeedAPI'
