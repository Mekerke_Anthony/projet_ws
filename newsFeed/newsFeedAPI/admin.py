from django.contrib import admin

from .models import Author, Article, Source, Doc
# Register your models here.

admin.site.register(Author)
admin.site.register(Article)
admin.site.register(Source)
admin.site.register(Doc)
