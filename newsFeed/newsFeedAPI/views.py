from django.shortcuts import get_object_or_404, get_list_or_404, render
from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .serializers import AuthorSerializer, SourceSerializer, ArticleSerializer
from .models import Author, Source, Article, Doc

class AuthorView(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['last_name', 'first_name', 'article__title']

    @action(detail=True, methods=['GET'])
    def articles(self, request, pk):
        author = self.get_object()
        article = Article.objects.filter(author=author)
        serializer = ArticleSerializer(article, many=True)
        return Response(serializer.data, status=200)

class SourceView(viewsets.ModelViewSet):
    queryset = Source.objects.all()
    serializer_class = SourceSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['name', 'website']

class ArticleView(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['title', 'author__last_name']

    @action(detail=True, methods=['GET'])
    def author(self, request, pk):
        article = self.get_object()
        author = Author.objects.filter(article=article)
        serializer = AuthorSerializer(author, many=True)
        return Response(serializer.data, status=200)

def docs(request):
    docs = Doc.objects.all()
    return render(request, 'mainsite/layout_page.html', {'title_page': 'NewsFeedAPI Documentation',
                                                            'page': 'newsFeedApi/docs.html',
                                                            'host': 'http://127.0.0.1:8000',
                                                            'docs': docs})