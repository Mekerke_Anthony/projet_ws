from django.shortcuts import get_object_or_404, get_list_or_404, render
from django.http import HttpResponse
import requests

from newsFeedAPI.models import Article
from newsFeedAPI.models import Source
from newsFeedAPI.models import Author


def index(request):
    list_articles = get_list_or_404(Article.objects.order_by('-publication_date'))
    ext_articles = requests.get('https://api.breakingapi.com/news?q=climate&type=headlines&locale=en-US&api_key=66406E7E3FAC4FA68DBD082E05DF04B1')
    #print(ext_articles.text)
    ext_articles_json = ext_articles.json()['articles']
    return render(request, 'mainsite/layout_page.html', {'title_page': 'NewsFeed',
                                                            'page': 'mainsite/index.html',
                                                            'list': list_articles,
                                                            'ext_articles': ext_articles_json})


def view_article(request, article_id):
    """
    :param request: article
    :param article_id: article.id
    :return: article with article_id
    """
    article = get_object_or_404(Article, id=article_id)

    return render(request, 'mainsite/layout_page.html', {'title_page': article.title,
                                                            'page': 'mainsite/article.html',
                                                            'article': article})


def view_source(request, source_id):
    list_articles = get_list_or_404(Article.objects.order_by('-publication_date'), source=source_id)

    return render(request, 'mainsite/layout_page.html', {'title_page': 'NewsFeed - source',
                                                            'page': 'mainsite/index.html',
                                                            'list': list_articles})


def view_author(request, author_id):
    list_articles = get_list_or_404(Article.objects.order_by('-publication_date'), author=author_id)

    return render(request, 'mainsite/layout_page.html', {'title_page': 'NewsFeed - author',
                                                            'page': 'mainsite/index.html',
                                                            'list': list_articles})


def search_title(request):
    list_articles = get_list_or_404(Article.objects.filter(title__contains=request.POST['title']))

    return render(request, 'mainsite/layout_page.html', {'title_page': 'NewsFeed - search title',
                                                            'page': 'mainsite/index.html',
                                                            'list': list_articles})


def search(request):
    articles = get_list_or_404(Article)
    authors = get_list_or_404(Author)
    sources = get_list_or_404(Source)

    date = {'month': range(13), 'day': range(32), 'year': ['2020', '2019', '2018']}

    objects = {'articles': articles, 'authors': authors, 'sources': sources, 'date': date}

    return render(request, 'mainsite/layout_page.html', {'title_page': 'NewsFeed - search',
                                                            'page': 'mainsite/form.html',
                                                            'objects': objects})


def results(request):

    entry_articles = Article.objects.all()

    if request.POST['input_title'] != 'Enter title':
        entry_articles = entry_articles.filter(title__contains=request.POST['input_title'])

    if request.POST['select_author'] != 'default':
        entry_articles = entry_articles.filter(author=request.POST['select_author'])

    if request.POST['select_source'] != 'default':
        entry_articles = entry_articles.filter(source=request.POST['select_source'])

    if request.POST['select_year_publication'] != 'default':
        entry_articles = entry_articles.filter(publication_date__year=request.POST['select_year_publication'])

    if request.POST['select_day_publication'] != 'default':
        entry_articles = entry_articles.filter(publication_date__day=request.POST['select_day_publication'])

    if request.POST['select_month_publication'] != 'default':
        entry_articles = entry_articles.filter(publication_date__month=request.POST['select_month_publication'])

    list_articles = list(entry_articles)

    return render(request, 'mainsite/layout_page.html', {'title_page': 'newsFeed - results',
                                                            'page': 'mainsite/index.html',
                                                            'list': list_articles})
