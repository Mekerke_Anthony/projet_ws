from django.urls import path
from . import views

app_name = 'mainsite'
urlpatterns = [
    path('search_title/', views.search_title, name='search_title'),
    path('results/', views.results, name='results'),
    path('search/', views.search, name='search'),
    path('source/<int:source_id>', views.view_source, name='source'),
    path('author/<int:author_id>', views.view_author, name='author'),
    path('article/<int:article_id>', views.view_article, name='article'),
    path('', views.index, name='index'),
]
