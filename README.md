## Procedure pour configurer le projet

Installer environnement virtuel à la racine du dossier cloné:
- python3 -m venv .

Activer environnement virtuel:
- source bin/activate

Installer les paquets necessaires:
- pip install -r requirement.txt

Lancer l'application:
- cd newsFeed/
- python3 manage.py runserver
- Aller à l'adresse: http://127.0.0.1:8000

Pour sortir de l'environnement virtuel:
- deactivate

admin: 
- login: newsfeed
- password: newsfeed
- email: pas d'email
